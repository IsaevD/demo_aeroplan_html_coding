$(window).load(function() {
    ymaps.ready(init);
});

function init() {
    myMap = new ymaps.Map("interactive_map", {
        center: [56.8856, 53.2554],
        zoom: 16
    });
}

$(function(){
   // initScrollUpDown();
    //cloudAnimation();
    initMap();
    initMenu();

    $("a.clicker").click(function() {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top + "px"
        }, {
            duration: 500
        });
        return false;
    });
    initScroll();

});

function makeNewPosition(){
    // Get viewport dimensions (remove the dimension of the div)
    var h = 10;
    var w = 10;
    var nh = Math.floor(Math.random() * h);
    var nw = Math.floor(Math.random() * w);
    return [nh,nw];
}
function cloudAnimation(){
    var newq = makeNewPosition();
    $('.cloud_animation').animate({ top: newq[0], left: newq[1] }, 1910, function(){
        cloudAnimation();
    });
};

function initMap() {
    $(".map_table td.number").click(function(){
        $(".map .item").hide();
        $(".map .number").show();
        $(".map_table td").removeClass("active");
        $(this).addClass("active");
        $(".map div[data-id='"+$(this).data("id")+"']").children(".number").hide();
        $(".map div[data-id='"+$(this).data("id")+"']").children(".item").show();
    });
    $(".map_table td.item").click(function(){
        $(".map .item").hide();
        $(".map .number").show();
        $(".map_table td").removeClass("active");
        $(this).prev().addClass("active");
        $(".map div[data-id='"+$(this).prev().data("id")+"']").children(".number").hide();
        $(".map div[data-id='"+$(this).prev().data("id")+"']").children(".item").show();
    });
}

function initMenu() {
    $(".menu li").click(function(){
        $(".menu li").removeClass("active");
        $(this).addClass("active");
        $(".articles li").hide();
        $(".articles li[data-id='"+$(this).data("id")+"']").show();
    });
}

function initScroll() {
    $(window).on("scroll", function(){
        $(".slider_switcher li a").removeClass("active");
        $($(".slider_switcher li")[Math.round($(this).scrollTop()/$(".slide").first().height())]).children("a").addClass("active");
    });
}

/*
function initScrollUpDown() {
    var lastScrollTop = 0;
    var flag = true;

    $(window).on("scroll", function(event){
        if (flag) {
            flag = false;
            currentScrollTop = $(this).scrollTop();
            if (lastScrollTop < currentScrollTop) {
                $(".slide.active").removeClass("active")
                    .next().addClass("active");
                $("html,body").animate({
                    scrollTop: $(window).scrollTop() + 100
                }, 1000, function(){ flag = true; });
            } else {
                $(".slide.active").removeClass("active")
                    .prev().addClass("active");
                $("html,body").animate({
                    scrollTop: $(window).scrollTop() - 100
                }, 1000, function(){ flag = true; });
            }
            lastScrollTop = currentScrollTop;
        }

    });

}



function scrollSlide(direction, height, flag) {
    if (direction == "up")
        $("html,body").animate({
            scrollTop: $(window).scrollTop() + height
        }, 1000, function(){ flag = true; });
    else
        $("html,body").animate({
            scrollTop: $(window).scrollTop() - height
        }, 1000, function(){ flag = true; });
}
    */