$(function(){
    initMenu();
    initMenu2();
    initSlide();
});

function initMenu() {
    $(".menu").click(function(){
        if ($(".menu_list").is(":visible")) {
            $(".menu_list").hide();
            $(".menu_bg").hide();
        } else {
            $(".menu_list").show();
            $(".menu_bg").show();
        }
    });

    $(".menu_list a").click(function(){
        $(".menu_list").hide();
        $(".menu_bg").hide();
        return true;
    });

    $(".menu_bg").click(function(){
        $(".menu_list").hide();
        $(".menu_bg").hide();
    });
}

function initMenu2() {
    $(".menu li").click(function(){
        $(".menu li").removeClass("active");
        $(this).addClass("active");
        $(".articles li").hide();
        $(".articles li[data-id='"+$(this).data("id")+"']").show();
    });
}

function initSlide() {
    $(".text_switcher .label").click(function() {
        $(".text_switcher li").removeClass("active");
        $(this).parent().addClass("active");
        return true;
    });
}